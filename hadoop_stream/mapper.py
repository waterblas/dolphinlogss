#! /usr/bin/env python
"""A more advanced Mapper, using Python iterators and generators."""

import sys
## key date+loacl+channel+pv

HEADER = ['Time', 'Package', 'Locale', 'From', 'Did', 'Action', 'Feature', 'Item', 'Count', 'Extra', 'Error']
TOP_STORY = 'io.topstory.news'
ALL_PV = 'all_pv'
ALL_REFRESH = 'all_refresh'
RCMD_PV = 'rcmd_pv'
RCMD_UV = 'rcmd_uv'
CATTAB_PV = 'cattab_pv'
CATTAB_UV = 'cattab_uv'
DISCOVER_PV = 'discover_pv'
DISCOVER_REFRESH = 'discover_refresh'
SUB_PV = 'sub_pv'
SUB_REFRESH = 'sub_refresh'


def read_input(file, separator):
    for line in file:
        ele = line.split(separator)
        # split the line into words
        data = {HEADER[i]: (ele[i]).strip() for i in range(0, len(ele))}
        if data.get('Package', None) != TOP_STORY:
            continue
        yield data
       

def _get_stat_type(row):
    res = []
    action = row.get('Action', None)
    feature = row.get('Feature', None)
    if action is None:
        return None
    tag = action.split('.',1)[0]
    # count pv:
    if action == 'news.click':
        # all news pv
        res.append(ALL_PV)
        # discover pv
        if feature in ['discover', 'discover.search']:
            res.append(DISCOVER_PV)
        # sub pv
        elif feature.split('.',1)[0] == 'sub':
            res.append(SUB_REFRESH)
        # rmcdlist pv
        elif feature == 'news.rcmdlist':
            res.append(RCMD_PV)
        # cattab pv
        elif feature == 'news.cattab':
            res.append(CATTAB_PV)
    # count news refresh
    elif action == 'news.refresh':
        res.append(ALL_REFRESH)
        if feature == 'news.rcmdlist':
            res.append(RCMD_UV)
        # cattab pv
        elif feature == 'news.cattab':
            res.append(CATTAB_UV)
    # count discover refresh
    elif tag == 'discover':
        res.append(DISCOVER_REFRESH)
    # count sub refresh
    elif tag == 'sub':
        res.append(SUB_REFRESH)
    return res

def _get_key(row):
    time_str = row.get('Time', None)
    locale = row.get('Locale', None)
    did = row.get('Did', None)
    if time_str is None or locale is None or did is None:
        return None
    current_date = time_str.split(' ', 1)[0]
    stat_type = _get_stat_type(row)
    if stat_type is None:
        return None
    return [['_'.join([current_date, locale, ele]), did] for ele in stat_type]


def main(separator='\t'):
    # input comes from STDIN (standard input)
    data = read_input(sys.stdin, separator)
    for row in data:
        for key, did in _get_key(row):
            print '%s%s%s' % (key, separator, did)


if __name__ == "__main__":
    main()