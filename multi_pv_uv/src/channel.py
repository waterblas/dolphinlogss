__author__ = 'ldy'

field_package = 'Package'
field_local = 'Locale'
field_feature = 'Feature'
field_did = 'Did'
field_action = 'Action'
local_col = ['ru-ru']
feature_col = ['rcmdlist', 'cattab', 'all']
user_group_col = ['all', 'new', 'old']


def previous_check(row, _local):
    if field_package not in row and field_local not in row:
        print 'warning row: %s' % row
        return False
    if row[field_package] != 'io.topstory.news':
        return False
    if row[field_local] != _local:
        return False
    return True


def _get_feature(row):
    if field_feature in row:
        temp = row[field_feature].split('.')
        if len(temp) < 2:
            return temp[0]
        return temp[1]
    return None


def _get_action(row):
    if field_action in row:
        return row[field_action].split('.')
    return None


class NewsData(object):
    def __init__(self, _date, _tab, _group, _local='ru-ru'):
        self.local = _local
        self.log_date = _date
        self.tabs = _tab
        self.user_group = _group
        self.pv = 0
        self.uv = 0
        self.pv_uv = -1   


class News(object):
    """
    docstring for News: count news pv uv
    :return {'ru-ru_all_all':0, 'ru-ru_cattab_new': 0}
    """

    def __init__(self, date, _newer_set):
        self.newer_set = _newer_set
        self.res = {}
        self.uv_tags = {}
        self.template = '%s_%s_%s'
        for local in local_col:
            for feature in feature_col:
                for user_group in user_group_col:
                    key = '_'.join([local, feature, user_group])
                    self.res[key] = NewsData(date, feature, user_group)

    def _count_pv(self, row, feature):
        self.res[self.template % (row[field_local], 'all', 'all')].pv += 1
        if feature in feature_col:
            self.res[self.template % (row[field_local], feature, 'all')].pv += 1
            if field_did in row and row[field_did] in self.newer_set:
                self.res[self.template % (row[field_local], feature, 'new')].pv += 1
        if field_did in row and row[field_did] in self.newer_set:
            self.res[self.template % (row[field_local], 'all', 'new')].pv += 1

    def _count_refresh(self, row, feature):
        """
        :param row:

        uv_tags = {'didstring': set['rcmdlist', 'new'] }
        """
        if field_did not in row:
            print "did can't be found"
            return
        did = row[field_did]
        if did in self.uv_tags:
            personal_tags = self.uv_tags[did]
            personal_tags.add(row[field_local])
            personal_tags.add(feature)
        else:
            self.uv_tags[did] = set((row[field_local], feature))
        if did in self.newer_set:
            self.uv_tags[did].add('new')

    def _count_uv(self):
        for did in self.uv_tags:
            local = (set(local_col) & self.uv_tags[did]).pop()
            self.res[self.template % (local, 'all', 'all')].uv += 1
            for feature in feature_col:
                if feature in self.uv_tags[did]:
                    self.res[self.template % (local, feature, 'all')].uv += 1
                    for group in user_group_col:
                        if group in self.uv_tags[did]:
                            self.res[self.template % (local, feature, group)].uv += 1
            for group in user_group_col:
                if group in self.uv_tags[did]:
                    self.res[self.template % (local, 'all', group)].uv += 1

    def count(self, row):
        action_list = _get_action(row)
        if len(action_list) < 2:
            return
        feature = _get_feature(row)
        action = action_list[1]
        if field_local not in row or row[field_local] not in local_col:
            return
        if action == 'click':
            self._count_pv(row, feature)
        elif action == 'refresh':
            self._count_refresh(row, feature)

    def _cal_old_data(self):
        for ele in self.res:
            ele_div = ele.split('_')
            if ele_div[2] == 'old':
                ele_div[2] = 'new'
                new_pv = self.res['_'.join(ele_div)].pv
                new_uv = self.res['_'.join(ele_div)].uv
                ele_div[2] = 'all'
                all_pv = self.res['_'.join(ele_div)].pv
                all_uv = self.res['_'.join(ele_div)].uv
                self.res[ele].pv = all_pv - new_pv
                self.res[ele].uv = all_uv - new_uv

    def _cal_pv_uv(self):
        temp = self.res
        for ele in temp:
            if temp[ele].uv != 0:
                temp[ele].pv_uv = temp[ele].pv * 1.0 / temp[ele].uv

    def get_result(self):
        self._count_uv()
        self._cal_old_data()
        self._cal_pv_uv()
        return self.res


class DiscoverData(object):
    def __init__(self, _date, _local):
        self.local = _local
        self.log_date = _date
        self.uv = 0
        self.pv = 0
        # self.banner_crt = -1
        # self.rank_crt = -1
        # self.hot_crt = -1


class Discover(object):
    def __init__(self, _date, _local='ru-ru'):
        self.res = DiscoverData(_date, _local)
        self.uv_set = set()
        self.banner_set = set()
        self.rank_set = set()
        self.hot_set = set()

    def count(self, row):
        did = row[field_did]
        if row[field_action] == 'news.click' \
                and (row[field_feature] == 'discover' or row[field_feature] == 'discover.search'):
            self.res.pv += 1
        action_list = _get_action(row)
        if action_list is not None and action_list[0] == 'discover':
            self.uv_set.add(did)
        # elif row[field_action] == 'banner.refresh':
        #     self.banner_set.add(did)
        # elif row[field_action] == 'rank.news.refresh':
        #     self.rank_set.add(did)
        # elif row[field_action] == 'search.hotkw.refresh':
        #     self.hot_set.add(did)

    def get_result(self):
        self.res.uv = len(self.uv_set)
        # if len(self.uv_set) > 0:
        #     self.res.banner_crt = len(self.banner_set) * 1.0 / len(self.uv_set)
        #     self.res.rank_crt = len(self.rank_set) * 1.0 / len(self.uv_set)
        #     self.res.hot_crt = len(self.hot_set) * 1.0 / len(self.uv_set)
        return self.res


class SubData(object):
    def __init__(self, _date, _local):
        self.local = _local
        self.log_date = _date
        self.pv = 0
        self.uv = 0
        self.pv_uv = -1


class Subscribe(object):
    def __init__(self, _date, _local='ru-ru'):
        self.uv_set = set()
        self.res = SubData(_date, _local)

    def count(self, row):
        feature = _get_feature(row)
        action_list = _get_action(row)
        if len(action_list) < 2:
            return
        if feature == 'sub' and action_list == ['news', 'click']:
            self.res.pv += 1
        self._add_uv_set(row, action_list)

    def _add_uv_set(self, row, action_list):
        if action_list[0] == 'sub':
            self.uv_set.add(row[field_did])

    def get_result(self):
        self.res.uv = len(self.uv_set)
        self.res.pv_uv = self.res.pv * 1.0 / self.res.uv if self.res.uv != 0 else -1
        return self.res
