"""
    table title: ['Time', 'Package', 'Locale', 'From', 'Did', 'Action', 'Feature', 'Item', 'Count', 'Extra', 'Error\n']
"""
import gzip
from datetime import date, timedelta
import MySQLdb
from channel import News, Subscribe, Discover, previous_check

db_host = 'localhost'
db_user = 'root'
db_pw = '12345'
db_name = "BI_stat"
log_dir = '/home/ldy/Documents/stat2/result/'
# log_dir = '/home/qwang/stat2/result/'
new_user_txt_dir = '/mnt/data_eg/new_user/new_user_'
res_dir = './res/'


def get_newer_set(_path):
    newer_set = set()
    with gzip.open(_path, 'r') as f_in:
        for line in f_in:
            try:
                newer_did = line[:-1]
                newer_set.add(newer_did)
            except:
                print 'user group line error: ', line
    return newer_set


def insert_res(cur, res, table_name):
    members = [attr for attr in dir(res[0]) if not callable(attr) and not attr.startswith("__")]
    members_str = ','.join(members)
    sql_src = "INSERT INTO %s (%s) VALUES (%s)" % (table_name, members_str, ','.join(['%s']*len(members)))
    # print sql_src
    temp = []
    for ele in res:
        temp.append(tuple([getattr(ele, x) for x in members]))
    cur.executemany(sql_src, temp)
    return temp


def __backup_res(format_res, filename):
    with open(res_dir + filename, 'a') as f_out:
        for ele in format_res:
            f_out.write('\t'.join(map(str, ele)) + '\n')


def save_res(sub, news, discover):
    db = MySQLdb.connect(host=db_host, user=db_user, passwd=db_pw, db=db_name)
    cur = db.cursor()
    if sub is not None:
        format_sub_res = insert_res(cur, sub, 'sub_channel')
        __backup_res(format_sub_res, 'sub_res.out')
    if news is not None:
        format_news_res = insert_res(cur, news, 'news_channel')
        __backup_res(format_news_res, 'news_res.out')
    if discover is not None:
        format_discover_res = insert_res(cur, discover, 'discover_channel')
        __backup_res(format_discover_res, 'discover_res.out')
    db.commit()
    db.close()


def stat_run(_stat_date, _log_path, _new_user_txt_path):
    news = News(_stat_date, get_newer_set(_new_user_txt_path))
    sub = Subscribe(_stat_date)
    discover = Discover(_stat_date)
    tr_sub = Subscribe(_stat_date, 'tr-tr')
    tr_discover = Discover(_stat_date, 'tr-tr')
    with open(_log_path, 'r') as f_in:
        print 'extract %s parsed log' % log_name_date
        table_title = f_in.readline().split('\t')
        print table_title
        for line in f_in:
            temp = line.split('\t')
            row_t = {table_title[i]: temp[i] for i in range(0, len(temp))}
            if previous_check(row_t, 'ru-ru'):
                news.count(row_t)
                sub.count(row_t)
                discover.count(row_t)
            elif previous_check(row_t, 'tr-tr'):
                tr_sub.count(row_t)
                tr_discover.count(row_t)
    res_sub = [sub.get_result(), tr_sub.get_result()]
    res_news = news.get_result()
    res_discover = [discover.get_result(), tr_discover.get_result()]
    save_res(res_sub, res_news.values(), res_discover)

if __name__ == '__main__':
    # for j in range(27, 0, -1):
        diff = 0
        log_name_date = (date.today() - timedelta(days=diff)).strftime('%Y%m%d')
        stat_date = (date.today() - timedelta(days=diff+1)).strftime('%Y-%m-%d')
        new_user_txt_path = new_user_txt_dir + stat_date + '_ru-ru.txt.gz'
        log_path = log_dir + log_name_date + '.parsed'
        stat_run(stat_date, log_path, new_user_txt_path)

