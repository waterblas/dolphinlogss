--id  log_date	local	tabs		user_group  pv	uv 	pv/uv
--			    ru-ru	rcmdlist	all
--			    tr-tr	cattab		new
--					    all			old

CREATE DATABASE data;

CREATE TABLE news_channel(
    id int unsigned not null auto_increment primary key,
    log_date date not null,
    local char(8) not null,
    tabs varchar(20) not null,
    user_group char(10) not null,
    pv int not null default 0,
    uv int not null default 0,
    pv_uv float(7,4) not null,
    INDEX index_tabs (tabs),
    INDEX index_user_group (user_group)
);
CREATE TABLE sub_channel(
    id int unsigned not null auto_increment primary key,
    log_date date not null,
    local char(8) not null,
    pv int not null default 0,
    uv int not null default 0,
    pv_uv float(7,4) not null
);
CREATE TABLE discover_channel(
    id int unsigned not null auto_increment primary key,
    log_date date not null,
    local char(8) not null,
    pv int not null default 0,
    uv int not null default 0
--    banner_crt float(7,4) not null,
--    rank_crt float(7,4) not null,
--    hot_sub_crt float(7,4) not null
);
