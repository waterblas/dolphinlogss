CREATE database data;
USE data
CREATE TABLE cattab_category(
    id INT unsigned not null auto_increment primary key,
    target_date DATE NOT NULL,
    locale char(8) NOT NULL DEFAULT 'ru-ru',
    category_id int NOT NULL DEFAULT 0,
    pv int NOT NULL DEFAULT 0,
    uv int NOT NULL DEFAULT 0,
    pv_uv float NOT NULL DEFAULT 0
);
