import sys
sys.path.append('../inactive_track/src/')
from pymongo import MongoClient
from datetime import date, timedelta
from inactive_conf import data_path, insert as db_insert

insert_str = "INSERT INTO cattab_category (target_date,category_id,uv,pv,pv_uv) VALUE (%s,%s,%s,%s,%s) "
parsed_header = ['Time', 'Package', 'Locale', 'From', 'Did', 'Action', 'Feature', 'Item', 'Count', 'Extra', 'Error']
con = MongoClient('52.28.22.114')
weibo_db = con['weibo_ru_ru']
category_dict = {}


def get_category(info_id):
    if info_id in category_dict:
        return category_dict[info_id]
    info = weibo_db.infos.find_one({'_id': info_id}, {'category': 1})
    category_dict[info_id] = info['category'] if info is not None else None 
    return category_dict[info_id]


def _get_each_line(file_path):
    print file_path
    with open(file_path, 'r') as f_in:
        for line in f_in:
            row = line.strip().split('\t')
            row = {parsed_header[i]: row[i] for i in range(len(row))}
            row_time = row.get('Time', False)
            row_locale = row.get('Locale', False)
            row_did = row.get('Did', False)
            row_action = row.get('Action', False)
            row_feature= row.get('Feature', False)
            row_item = row.get('Item', False)
            row_count= row.get('Count', False)
            if row_locale != 'ru-ru' and row_did and row_action and row_feature:
                yield None
            if row_feature == 'news.cattab' and row_item and row_action in ['news.click', 'news.refresh']:
                yield {'cattab': row }
            elif row_feature == 'news.morelist' and row_item and row_action in ['new.click', 'nes.refresh']:
                yield {'morelist': row}
            else:
                yield None


def count_cattab(file_path):
    cattab_dict = {}
    for ele in _get_each_line(file_path):
        if ele is None or'cattab' not in ele:
            continue
        row = ele['cattab']
        category_id = get_category(int(row['Item']))
        if category_id is None:
            continue
        if category_id not in cattab_dict:
            cattab_dict[category_id] = {'pv': 0, 'uv_set': set(), 'uv': 0, 'pv_uv': -1}
        if row['Action'] == 'news.click':
            cattab_dict[category_id]['pv'] += 1
        elif row['Action'] == 'news.refresh':
            cattab_dict[category_id]['uv_set'].add(row['Did'])
    for ele in cattab_dict:
        category = cattab_dict[ele]
        category['uv'] = len(category['uv_set'])
        category['pv_uv'] = category['pv'] / category['uv'] if category['uv'] != 0 else -1

    return cattab_dict


def _format_res(res, format_date):
    format_res = []
    for ele in res:
        format_res.append([format_date, ele, res[ele]['uv'], res[ele]['pv'], res[ele]['pv_uv']])
    return format_res


def daily(target_date=date.today()):
    format_date = target_date.strftime('%Y%m%d')
    file_path = data_path['parsed'] + '%s.parsed.bak' % format_date
    res = _format_res(count_cattab(file_path), format_date)
    db_insert(res, insert_str, 'data')


if __name__ == '__main__':
    daily(date.today() - timedelta(days=32))
