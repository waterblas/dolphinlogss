__author__ = 'ldy'
from datetime import date, timedelta

log_dir = '/home/ldy/Documents/stat2/result/'
#log_dir = '/home/qwang/stat2/result/'
field_package = 'Package'
field_local = 'Locale'
field_feature = 'Feature'
field_did = 'Did'
field_action = 'Action'


def stat_run(_stat_date, _log_path):
    ru_uv_all_set = set()
    ru_uv_click_dict = {}
    tr_uv_all_set = set()
    tr_uv_click_dict = {}
    with open(_log_path, 'r') as f_in:
        print 'extract %s parsed log' % log_name_date
        table_title = f_in.readline().split('\t')
        print table_title
        for line in f_in:
            temp = line.split('\t')
            row_t = {table_title[j]: temp[j] for j in range(0, len(temp))}
            if field_package not in row_t and field_local not in row_t:
                print 'warning row: %s' % row_t
                continue
            if row_t[field_package] != 'io.topstory.news':
                continue
            if row_t[field_local] == 'ru-ru':
                if 'Did' in row_t:
                    ru_uv_all_set.add(row_t['Did'])
                    if row_t['Action'] == 'news.click':
                        if row_t['Did'] in ru_uv_click_dict:
                            ru_uv_click_dict[row_t['Did']] += 1
                        else:
                            ru_uv_click_dict[row_t['Did']] = 1
            elif row_t[field_local] == 'tr-tr':
                if 'Did' in row_t:
                    tr_uv_all_set.add(row_t['Did'])
                    if row_t['Action'] == 'news.click':
                        if row_t['Did'] in tr_uv_click_dict:
                            tr_uv_click_dict[row_t['Did']] += 1
                        else:
                            tr_uv_click_dict[row_t['Did']] = 1

    with open('./ru-ru_uv_click.csv', 'a') as f_out:
        f_out.write('%s,%s,%s\n' % (_stat_date, len(ru_uv_all_set), len(ru_uv_click_dict)))
    with open('./tr-tr_uv_click.csv', 'a') as f_out:
        f_out.write('%s,%s,%s\n' % (_stat_date, len(tr_uv_all_set), len(tr_uv_click_dict)))

    tr_d = [0]*16
    ru_d = [0]*16
    for key in tr_uv_click_dict:
        val = tr_uv_click_dict[key]
        if val > 15:
            tr_d[15] += 1
        else:
            tr_d[val-1] += 1

    for key in ru_uv_click_dict:
            val = ru_uv_click_dict[key]
            if val > 15:
                ru_d[15] += 1
            else:
                ru_d[val-1] += 1
    with open('./ru-ru_distribute.csv', 'a') as f_out:
        f_out.write(_stat_date + ',' + ','.join(str(x) for x in ru_d) + '\n')
    with open('./tr-tr_distribute.csv', 'a') as f_out:
        f_out.write(_stat_date + ',' + ','.join(str(x) for x in tr_d) + '\n')


if __name__ == '__main__':
    for i in range(27, -1, -1):
        diff = i
        log_name_date = (date.today() - timedelta(days=diff)).strftime('%Y%m%d')
        stat_date = (date.today() - timedelta(days=diff+1)).strftime('%Y-%m-%d')
        log_path = log_dir + log_name_date + '.parsed'
        stat_run(stat_date, log_path)
