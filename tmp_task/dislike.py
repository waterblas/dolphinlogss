# import time
import json
# from datetime import datetime, timedelta
import gzip
import xlwt

user_path = './user.log.%s.gz'
format_rule = ['-', '-', '-', 'ip', '-', 'date', 'time', '-', '-', 'get', '-', 'post']
span = ['bor', 'inap', 'old', 'dup', 'source']


def extract_info(line):
    try:
        res = {}
        eles = line.split(' ')
        eles = [x for x in eles if x != '']
        for i in range(len(format_rule)):
            if format_rule[i] != '-':
                res[format_rule[i]] = eles[i]
        return res
    except:
        print 'extract error: %s' % line
        return None


def confirt_date(res):
    try:
        # x = time.strptime(res['time'].split(',')[0], '%H:%M:%S')
        # if x > time.strptime('00:01:00,000'.split(',')[0], '%H:%M:%S'):
        #     return res['date']
        # current_day = datetime.strptime(res['date'], "%Y-%m-%d").date() + timedelta(days=1)
        # return current_day.strftime('%Y-%m-%d')
        return res['date']
    except:
        print 'date  error: %s' % res
        return None


def get_local(res):
    try:
        return res['get'].split('lc=')[1][0:5]
    except:
        print 'local missing: %s' % res
        return None


def get_unitd(res):
    try:
        post = json.loads(res['post'][:-1])
        if 'unitd' in post:
            return post['unitd']
    except:
        print 'unitd error: %s' % res
        return None


def set_unitd(unitd_dict, current_date, unitd_info):
    news_id = unitd_info['id']
    for ele in unitd_info['reason']:
        if ele not in span:
            continue
        key_name = '_'.join([current_date, str(news_id), ele])
        if key_name in unitd_dict:
            unitd_dict[key_name] += 1
        else:
            unitd_dict[key_name] = 1


ru_unitd_dict = {}
tr_unitd_dict = {}
for j in range(2, 27):
    with gzip.open(user_path % j, 'r') as f_in:
        for line in f_in:
            res = extract_info(line)
            if res is None:
                continue
            local = get_local(res)
            unitd_info = get_unitd(res)
            current_date = confirt_date(res)
            if unitd_info is None or local is None or current_date is None:
                continue
            if local == 'ru-ru':
                set_unitd(ru_unitd_dict, current_date, unitd_info)
            elif local == 'tr-tr':
                set_unitd(tr_unitd_dict, current_date, unitd_info)

print 'done'

book = xlwt.Workbook()
sheet1 = book.add_sheet('ru-ru')
sheet2 = book.add_sheet('tr-tr')
sheet1.write(0, 0, 'Date')
sheet1.write(0, 1, 'Id')
sheet1.write(0, 2, 'Reason')
sheet1.write(0, 3, 'Times')
sheet2.write(0, 0, 'Date')
sheet2.write(0, 1, 'Id')
sheet2.write(0, 2, 'Reason')
sheet2.write(0, 3, 'Times')


num = 0
for k in ru_unitd_dict:
    ele = k.split('_')
    num += 1
    col = 0
    for t in range(len(ele)):
        col += 1
        sheet1.write(num, t, ele[t])
    sheet1.write(num, col, ru_unitd_dict[k])

num = 0
for k in tr_unitd_dict:
    ele = k.split('_')
    num += 1
    col = 0
    for t in range(len(ele)):
        col += 1
        sheet2.write(num, t, ele[t])
    sheet2.write(num, col, tr_unitd_dict[k])
book.save('unitd.xls')
