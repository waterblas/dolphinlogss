from datetime import datetime
from base import LogBaseParser


class AccessLogParser(LogBaseParser):
    '''
    Parse log by format.
    '''
    def _parse_line(self, line):
        '''parse line of log'''
        columns = line.split(' ')
        msg = {}
        for i in range(len(self.log_format)):
            name = self.log_format[i]
            if name != '-' and hasattr(self, '_parse_%s' % name):
                method = getattr(self, '_parse_%s' % name)
                method(msg, columns[i])
        self._normalize(msg)
        return msg

    def _parse_url(self, msg, url):
        '''parse string like decode of url'''
        self._parse_api(msg, url)

    def _parse_time(self, msg, time_str):
        '''parse time string to time_obj'''
        time_obj = datetime.strptime(time_str, '[%d/%b/%Y:%H:%M:%S')
        converted_str = time_obj.strftime('%Y-%m-%d %H:%M:%S')
        msg['__time'] = converted_str
        msg['__datetime'] = time_obj

    def _parse_code(self, msg, column):
        '''parse http code'''
        pass
#        msg['__code'] = int(column)
