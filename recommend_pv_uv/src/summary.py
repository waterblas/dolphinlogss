# -*- coding: utf-8 -*-

import os
from datetime import date, timedelta
import codecs
import gzip
import sqlite3
import csv

ACTIVE_USER = 1
NOT_ACTIVE_USER = 0


class Summary(object):
    """ csv output:
    Date    local   活跃度  UV  PV  Refresh PV/UV   CTR
    2015-07-20  ru-ru   活跃    1000    10000   20000   10  0.5

    Date    local   新老用户    UV  PV  Refresh PV/UV   CTR
    2015-07-20  ru-ru   new 1000    10000   20000   10  0.5
    2015-07-20  ru-ru   old 1000    10000   20000   10  0.5
    """
    def __init__(self, _db_path, _new_user_list_dir, _yesterday=date.today() - timedelta(days=1), _temp_dir='../data_eg/', _res_dir='../res/'):
        self.db_path = _db_path
        self.temp_dir = _temp_dir
        self.res_dir = _res_dir
        self.new_user_list_dir = _new_user_list_dir
        self.user_dict = {}
        self.count_result = {}
        self.file_name_list = ['uv.csv', 'refresh.csv', 'pv.csv']
        self.sum_sql = "SELECT SUM(day_1 + day_2 + day_3 + day_4 + day_5 + day_6 + day_7 + day_8 + day_9 + " \
                  "day_10 + day_11 + day_12 + day_13 + day_14) FROM access_log WHERE did=?"
        self.res_files = {'by_active': 'active', 'by_new': 'new_old'}
        self.active_tags = {'active': '活跃', 'not_active': '非活跃'}
        self.new_tags = {'new': '新用户', 'old': '老用户'}
        self.fieldnames_active = ['Date', 'local', '活跃度', 'uv', 'pv', 'refresh', 'pv/uv', 'CTR']
        self.fieldnames_new = ['Date', 'local', '新老用户', 'uv', 'pv', 'refresh', 'pv/uv', 'CTR']
        self.yesterday_str = _yesterday.strftime("%Y-%m-%d")

    def __search_db(self, cur, did):
        """
            note:  '\n' after each line of the .csv file
        """
        cur.execute(self.sum_sql, (did[:-1],))
        row = cur.fetchone()
        if row[0] is None:
            # print "did not find"
            return False
        if row[0] >= 30:
            self.user_dict[did] = ACTIVE_USER
            # print "find one > 30"
            return True
        else:
            # print "visit time %s" % row[0]
            self.user_dict[did] = NOT_ACTIVE_USER
            return False

    def __pre_data(self):
        con = sqlite3.connect(self.db_path)
        cur = con.cursor()
        print 'new user list day: %s' % self.yesterday_str
        for name in self.file_name_list:
            active_user_num = 0
            total_num = 0
            new_num = 0
            seen = set()
            with open(self.temp_dir + name, 'r') as f_in, \
                    gzip.open(self.new_user_list_dir + 'new_user_' + self.yesterday_str + '_ru-ru.txt.gz', 'r') as new_user:
                for line in new_user:
                    seen.add(line)
                for did in f_in:
                    total_num += 1
                    if did in self.user_dict:
                        if self.user_dict[did] == ACTIVE_USER:
                            active_user_num += 1
                    else:
                        if self.__search_db(cur, did):
                            active_user_num += 1
                    if did in seen:
                        new_num += 1
            print '%s  new %s, total %s' % (name, new_num, total_num)
            self.count_result[os.path.splitext(name)[0]] = {"active": active_user_num, "not_active": total_num - active_user_num, "new": new_num, "old": total_num - new_num}
        con.close()
        print self.count_result

    def generate(self):
        self.__pre_data()
        self.__by_active()
        self.__by_new_old()
        self._convert_to_gbk()
        pass

    def __by_active(self):
        with open(self.temp_dir + self.res_files['by_active'] + '.csv', 'a') as f_out:
            writer = csv.DictWriter(f_out, fieldnames=self.fieldnames_active, delimiter=',')
            # writer.writeheader()
            for tag in self.active_tags:
                line = {"Date": self.yesterday_str, "local": "ru-ru", "活跃度": self.active_tags[tag]}
                for col in self.count_result:
                    line[col] = self.count_result[col][tag]
                    pass
                line['pv/uv'] = "%.2f" % (line['pv'] * 1.0 / line['uv']) if line['uv'] != 0 else '-'
                line['CTR'] = "%.2f" % (line['pv'] * 1.0 / line['refresh']) if line['refresh'] != 0 else '-'
                writer.writerow(line)

    def __by_new_old(self):
        with open(self.temp_dir + self.res_files['by_new'] + '.csv', 'a') as f_out:
            writer = csv.DictWriter(f_out, fieldnames=self.fieldnames_new, delimiter=',')
            # writer.writeheader()
            for tag in self.new_tags:
                line = {"Date": self.yesterday_str, "local": "ru-ru", "新老用户": self.new_tags[tag]}
                for col in self.count_result:
                    line[col] = self.count_result[col][tag]
                line['pv/uv'] = "%.2f" % (line['pv'] * 1.0 / line['uv']) if line['uv'] != 0 else '-'
                line['CTR'] = "%.2f" % (line['pv'] * 1.0 / line['refresh']) if line['refresh'] != 0 else '-'
                writer.writerow(line)

    def _convert_to_gbk(self):
        for key in self.res_files:
            filename = self.res_files[key]
            dat = codecs.open(self.temp_dir + filename + '.csv', "r", "utf-8")
            new = codecs.open(self.res_dir + filename + '_' + self.yesterday_str + '.csv', "w", "gbk")
            for line in dat.readlines():
                new.write(line)
            dat.close()
            new.close()
