from time import time
from datetime import date
import gzip
from access import AccessLogParser
from log import DailyLog


class Task(object):

    def __init__(self, _log_format, _log_parser_con, _log_path_head, _day=date.today(), _temp_dir='../data_eg/'):
        self.AccessM = AccessLogParser(_log_format)
        self.FilterM = DailyLog(_log_parser_con)
        self.day_str = _day.strftime("%Y%m%d")
        self.log_path_head = _log_path_head
        self.temp_dir = _temp_dir

    def get_data(self):
        t0 = time()
        print 'running...'
        with gzip.open(self.log_path_head + self.day_str + '.gz', 'r') as log_io, \
            open(self.temp_dir + 'refresh.csv', 'w') as refresh_io, open(self.temp_dir + 'pv.csv', 'w') as pv_i:
            for line in log_io:
                msg = self.AccessM.parse(line)
                res = self.FilterM.days_run(msg[0])
                if res == 'REFRESH':
                    refresh_io.write(msg[0]['did'] + '\n')
                if res == 'PV':
                    pv_i.write(msg[0]['did'] + '\n')

        with open(self.temp_dir + 'refresh.csv', 'r') as refresh_o, open(self.temp_dir + 'uv.csv', 'w') as uv_i:
            seen = set()
            for line in refresh_o:
                seen.add(line)
            for line in seen:
                uv_i.write(line)
        t1 = time()
        print 'takes %f' %(t1-t0)

    def test(self):
        print 'testing ..'
        pv_num = 0
        refresh_num = 0
        with gzip.open(self.log_path_head + self.day_str + '.gz', 'r') as log_io:
            for line in log_io:
                msg = self.AccessM.parse(line)
                if 'pn' in msg and msg[0]['pn'] == 'io.topstory.news' and msg[0]['lc'] == 'ru-ru':
                    if msg[0]['__api'] == "/api/infostream/weibo.json":
                        pv_num += 1
                    elif msg[0]['__api'] == "/api/infostream/tab/infos.json":
                        refresh_num += 1
            print 'pv_num: %s refresh num: %s' % (pv_num, refresh_num)

