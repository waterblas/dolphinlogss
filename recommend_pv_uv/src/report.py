#!/usr/bin/python
# -*- coding: utf-8 -*-

import smtplib
from os.path import basename
from datetime import date, timedelta
from email.mime.application import MIMEApplication
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText
from email.utils import COMMASPACE, formatdate


def send_mail(send_from, send_to, subject, text, files=None,
              server='mail.bainainfo.com'):
    assert isinstance(send_to, list)
    msg = MIMEMultipart()
    msg['From'] = send_from
    msg['To'] = COMMASPACE.join(send_to)
    msg['Date'] = formatdate(localtime=True)
    msg['Subject'] = subject
    msg.attach(MIMEText(text))
    for f in files or []:
        with open(f, "rb") as fil:
            attach_file = MIMEApplication(fil.read())
            attach_file.add_header('Content-Disposition', 'attachment', filename=basename(f))
            msg.attach(attach_file)
    smtp = smtplib.SMTP(server)
    smtp.sendmail(send_from, send_to, msg.as_string())
    smtp.close()


def daily_report():
    today_str = (date.today() - timedelta(days=1)).strftime("%Y-%m-%d")
    res_files_path = ['../res/active_%s.csv' % today_str, '../res/new_old_%s.csv' % today_str]
    subject = 'access_log_' + today_str +'_推荐流统计'
    send_from = 'dyliang@bainainfo.com'
    send_to = ['dyliang@bainainfo.com',]
    text = 'Access log统计说明: 统计topstory的数据（pn=io.topstory.news），俄罗斯的数据（lc=ru-ru）。'
    send_mail(send_from, send_to, subject, text, res_files_path)
