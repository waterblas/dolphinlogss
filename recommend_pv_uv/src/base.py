import urlparse

_DEFAULT_LOCALE = 'zh-cn'
_DEFAULT_CN_PACKAGE = 'com.dolphin.browser.xf'
_DEFAULT_INT_PACKAGE = 'mobi.mgeek.TunnyBrowser'
UNKNOWN = 'unknown'
I18N = True
GROUP_ALL = 'All'


class Feature(object):
    '''Define news list and detail source'''
    UNKNOWN   =  -1  # invalid feature or missing feature parameter
    RECOMMEND =   1  # news list&detail from /api/infostream/list.json
    CATEGORY  =   2  # news list&detail from /api/infostream/latest.json on tab
    RELEVANCE =   3  # news list&detail from /relevance/list.json
    TOP       =   4  # news list&detail from /api/infostream/top.json
    SEARCH    =   5  # news list&detail from /api/infostream/search.json
    TABS      =   7  # /api/infostream/tabs.json
    LOADMORE  =   8  # loadmore on the bottom of news detail
    GALLERY   =  99  # gallery tab


def parse_msg(msg):
    '''
    parse one msg to msg list by group value
    '''
    if msg['group'] in range(1, 17):
        return [msg]
    else:
        msg['group'] = GROUP_ALL
        return [msg]


def get_locale(msg):
    '''get locale value'''
    if I18N:
        return msg['lc'] if 'lc' in msg else 'unknown'
    else:
        return _DEFAULT_LOCALE


def get_package(msg):
    '''get package name'''
    if I18N:
        return msg['pn'] if 'pn' in msg and msg['pn'] else UNKNOWN
    else:
        return _DEFAULT_CN_PACKAGE


class LogBaseParser(object):
    '''
    Parse log by format.
    '''
    def __init__(self, log_format):
        self.log_format = log_format

    def parse(self, line):
        '''
        Parse line and return a dict containing line information.
        '''
        msg = self._parse_line(line)
        msgs = parse_msg(msg)
        return msgs

    def _parse_api(self, msg, url):
        '''parse string like decode of url'''
        parse_result = urlparse.urlparse(url)
        message = dict(urlparse.parse_qsl(parse_result.query))
        msg['__api'] = parse_result.path
        for key in message:
            if key in msg:
                raise KeyError('Duplicate key %s' % key)
            else:
                msg[key] = message[key]

    def _normalize(self, msg):
        msg['did'] = msg['did'] if 'did' in msg else UNKNOWN
        msg['lc'] = get_locale(msg)
        msg['package'] = get_package(msg)
        msg['group'] = None
        msg['from'] = msg['from'] if 'from' in msg else UNKNOWN
        msg['feature'] = msg['feature'] if 'feature' in msg else Feature.UNKNOWN
