#!/usr/bin/python
# -*- coding: utf-8 -*-

__author__ = 'ldy'

import sys
from datetime import date, timedelta, datetime
from daily import Task
from visit import DB14
from summary import Summary
from report import daily_report

LOG_FORMAT = ['ip', '-', '-', 'time', 'tz', 'method', 'url', 'code']
TOP_STORY_CONDITION = {'lc': 'ru-ru', 'pn': 'io.topstory.news'}
TEMP_DIR = '../data_eg/'
LOG_PATH_HEAD = '/mnt/logs/recommender-frankfurt/access.log-'
NEW_USER_FILE_DIR = '/mnt/data_eg/new_user/'
RESULT_DIR = '../res/'
DB_NAME = 'user_history_visit.db'
DIFF_DAYS = 1

if __name__ == "__main__":
    diff = DIFF_DAYS 
    argv = sys.argv[1:]
    db = DB14(LOG_FORMAT, TOP_STORY_CONDITION, LOG_PATH_HEAD, TEMP_DIR + DB_NAME)
    if argv and argv[0] == 'init':
        if len(argv) > 1:
            init_day = datetime.strptime(argv[1], "%Y-%m-%d").date()
        else:
            init_day = date.today()
        db.initialize(init_day)
    elif argv and argv[0] == 'update':
        diff = argv[1] if len(argv) > 1 else 1

    # 每天的新老用户列表对应其文件名日期
    special_day = date.today() - timedelta(days=diff)
    task_day = date.today() - timedelta(days=(diff-1))
    # log文件对应其文件名前一天的log
    task = Task(LOG_FORMAT, TOP_STORY_CONDITION, LOG_PATH_HEAD, task_day)
    summary = Summary(TEMP_DIR + DB_NAME, NEW_USER_FILE_DIR, special_day)
    db.update_date(special_day)
    task.get_data()
    summary.generate()
    daily_report()
