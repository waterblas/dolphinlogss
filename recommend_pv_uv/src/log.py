'''
    judge the log msg kind
'''
API_REFRESH = '/api/infostream/tab/infos.json'
API_CONTENT = '/api/infostream/weibo.json'
FEATURE_HOME = '1'
FEATURE_MORE = '8'


class DailyLog:
    def __init__(self, _conditions):
        self.conditions = _conditions
        self.user_pv_dict = {}
        self.user_count = {}

    def _pn_check(self, msg):
        if 'did' not in msg:
            return False
        for key, value in self.conditions.items():
            if (key not in msg) or msg[key] != value:
                return False
        return True

    def _count_refresh(self, msg):
        if not self._pn_check(msg):
            return False
        if '__api' in msg and msg['__api'] == API_REFRESH :
            if not (('tid'  and 'feature') in msg):
                return False
            if str(msg['tid']) == '0' and (str(msg['feature']) in [FEATURE_HOME , FEATURE_MORE ]):
                return True
        return False

    def _log_last_pv(self, _is, msg):
        dicts = self.user_pv_dict
        if 'did' not in msg:
            return
        did = msg['did']
        dicts[did] = _is

    def _is_from_pv(self, msg):
        if not self._pn_check(msg):
            return False
        if '__api' in msg and msg['__api'] == API_CONTENT and 'feature' in msg:
            if str(msg['feature']) == FEATURE_HOME:
                return True
            elif str(msg['feature']) == FEATURE_MORE and self._is_last_pv(msg):
                return True
        return False

    def _is_last_pv(self, msg):
        if 'did' not in msg:
            return False
        did = msg['did']
        if did in self.user_pv_dict and self.user_pv_dict[did]:
            return True
        return False

    def _count_pv(self, msg):
        is_pv = self._is_from_pv(msg)
        self._log_last_pv(is_pv,msg)
        if is_pv:
            return True
        return False

    def days_run(self, msg):
        if self._count_refresh(msg):
            return 'REFRESH'
        if self._count_pv(msg):
            return 'PV'
        return 'UNKNOW'

    def count_visit(self, msg):
        if not self._pn_check(msg):
            return
        if '__api' not in msg or msg['__api'] != API_CONTENT:
            return
        did = msg['did']
        if did in self.user_count:
            self.user_count[did] += 1
        else:
            self.user_count[did] = 1
        # print "did %s count %s" % (did, self.user_count[did])
