# -*- coding: utf-8 -*-

from time import time
from datetime import date, timedelta
import gzip
import sqlite3 as sqlite
from access import AccessLogParser
from log import DailyLog
PERIODS = 14


class DB14(object):

    def __init__(self, _log_format, _log_parser_con, _log_path_head, _db_path):
        self.db_path = _db_path
        self.log_path_head = _log_path_head
        self.AccessM = AccessLogParser(_log_format)
        self.sample_day = date(2015, 1, 1)
        self.periods = PERIODS
        self.log_parser_con = _log_parser_con
        self.table_create_sql = "CREATE TABLE access_log(id INTEGER PRIMARY KEY, did char UNIQUE NOT NULL, " \
                   "day_1 int DEFAULT 0, day_2 int DEFAULT 0,day_3 int DEFAULT 0,day_4 int DEFAULT 0,day_5 int DEFAULT 0," \
                   "day_6 int DEFAULT 0,day_7 int DEFAULT 0,day_8 int DEFAULT 0,day_9 int DEFAULT 0,day_10 int DEFAULT 0," \
                   "day_11 int DEFAULT 0,day_12 int DEFAULT 0,day_13 int DEFAULT 0,day_14 int DEFAULT 0)"

    def initialize(self, init_day=date.today()):
        con = sqlite.connect(self.db_path)
        print "init"
        with con:
            cur = con.cursor()
            cur.execute("SELECT name FROM sqlite_master WHERE type='table' AND name='access_log';")
            table_row = cur.fetchone()
            if table_row is None:
                cur.execute(self.table_create_sql)
                print "create db"
        for i in range(2, self.periods + 1):
            day = init_day - timedelta(days=i)
            self.update_date(day)

    def update_date(self, day=date.today() - timedelta(days=1)):
        con = sqlite.connect(self.db_path)
        with con:
            cur = con.cursor()
            t0 = time()
            remain_num = (day - self.sample_day).days % 14 + 1
            print "remain day from 2015-1-1 %s" % remain_num
            day_str = day.strftime("%Y%m%d")
            print "extract log day: %s" % day_str
            log = DailyLog(self.log_parser_con)
            with gzip.open(self.log_path_head + day_str + '.gz', 'r') as f_in:
                for line in f_in:
                    msg = self.AccessM.parse(line)
                    log.count_visit(msg[0])
            print "visit over"
            self.__write_db(log, cur, remain_num)
            t1 = time()
            print 'takes %f' % (t1-t0)

    def __write_db(self, log, cur, remain_num):
        cur.execute("UPDATE access_log SET day_" + str(remain_num) + "=0")
        for row in log.user_count:
            num = log.user_count[row]
            cur.execute("SELECT did FROM access_log WHERE did=?", (row,))
            did_row = cur.fetchone()
            if did_row is None:
                insert_sql = "INSERT INTO access_log(did,day_" + str(remain_num) + ") VALUES(?,?)"
                cur.execute(insert_sql, (row, num))
            else:
                update_sql = "UPDATE access_log SET day_" + str(remain_num) + "=? WHERE did=?"
                cur.execute(update_sql, (num, row))

