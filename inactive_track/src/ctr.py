import os
from datetime import date, timedelta
from inactive_conf import insert as db_insert, data_path

max_num = 10
insert_str = "INSERT INTO ctr (target_date, groups, ctr_1,ctr_2,ctr_3,ctr_4,ctr_5,ctr_6,ctr_7,ctr_8," \
             "ctr_9,ctr_10) VALUE (%s)" % ','.join(['%s']*(max_num+2))
parsed_header = ['Time', 'Package', 'Locale', 'From', 'Did', 'Action', 'Feature', 'Item', 'Count', 'Extra', 'Error']


class UserLog(object):
    """ record one user action log"""
    def __init__(self):
        # the news refresh back to click on that refresh action
        self.click = set()
        # the refresh action time, if refresh time is same with same person, it means the same count of refresh
        self.refresh_time = ''
        # this user refresh count num
        self.refresh_count = -1


def _get_parsed_files(files_folder):
    files_list = []
    for filename in os.listdir(files_folder):
        if filename.endwith('.parsed'):
            files_list.append(filename)
    return files_list


def _format_line(line, parsed_header):
    row = line.strip().split('\t')
    return {parsed_header[i]: row[i] for i in range(len(row))}


def _is_fit(row):
    if row.get('Feature', None) != 'news.rcmdlist':
        return False
    if row.get('Action', None) not in ['news.click', 'news.refresh']:
        return False
    if row.get('Did', None) is None:
        return False
    return True


def _log_refresh_action(row, sum_click_refresh, user_log_item, is_active):
    # count num condition
    if user_log_item.refresh_count >= max_num - 1 and row['Time'] != user_log_item.refresh_time:
        user_log_item.refresh_count = max_num
        return None
    if row['Time'] == user_log_item.refresh_time:
        user_log_item.click.add(row['Item'])  # refresh action return a list of news
    else:
        user_log_item.click = set()  # clean history, prepare next count
        user_log_item.click.add(row['Item'])
        user_log_item.refresh_count += 1   
        user_log_item.refresh_time = row['Time']
    if is_active:
        sum_click_refresh['active']['refresh'][user_log_item.refresh_count] += 1
    else:
        sum_click_refresh['inactive']['refresh'][user_log_item.refresh_count] += 1


def _log_click_action(row, sum_click_refresh, user_log_item, is_active):
    if row['Item'] in user_log_item.click and user_log_item.refresh_count != max_num:
        if is_active:
            sum_click_refresh['active']['click'][user_log_item.refresh_count] += 1
        else:
            sum_click_refresh['inactive']['click'][user_log_item.refresh_count] += 1
    

def count_next_crt(file_path, user_group):
    user_times = {}
    sum_click_refresh = {
        'inactive': {'click': [0] * max_num, 'refresh': [0] * max_num},
        'active': {'click': [0] * max_num, 'refresh': [0] * max_num}
    }
    with open(file_path, 'r') as f_in:
        for line in f_in:
            is_active = False
            row = _format_line(line, parsed_header)
            if not _is_fit(row) or row['Locale'] != 'ru-ru':
                continue
            did = row['Did']
            if did not in user_times:
                user_times[did] = UserLog()
            if did in user_group:
                is_active = True
            if row['Action'] == 'news.refresh':
                _log_refresh_action(row, sum_click_refresh, user_times[did], is_active)
            elif row['Action'] == 'news.click':
                _log_click_action(row, sum_click_refresh, user_times[did], is_active)

    return sum_click_refresh


def _get_user_group(group_file_path):
    active_group = set()
    with open(group_file_path, 'r') as f_in:
        for line in f_in:
            tag = line.strip().split('\t', 2)
            if tag[1] == '1':
                active_group.add(tag[0])
    return active_group


def _format_res(res, format_date):
    temp = []
    for group in res:
        ctr = [(res[group]['click'][i] * 1.0 / res[group]['refresh'][i]) if res[group]['refresh'][i] > 0 else -1
               for i in range(max_num)]
        temp.append([format_date, group] + ctr)
    return temp
        

def daily(target_date=date.today()):
    format_date = target_date.strftime('%Y%m%d')
    parsed_path = data_path['parsed'] + '%s.parsed' % format_date
    group_file_path = data_path['group'] + 'user_group-%s.txt' % format_date
    active_group = _get_user_group(group_file_path)
    stat_res = count_next_crt(parsed_path, active_group)
    format_res = _format_res(stat_res, (target_date - timedelta(days=1)).strftime('%Y%m%d'))
    print format_res
    db_insert(format_res, insert_str)


def init_db(num):
    for i in range(num):
        daily(date.today() - timedelta(days=i))
    

if __name__ == '__main__':
    daily()

