# -*- coding: utf-8 -*-
"""
    统计用户从成为新用户开始，到其不再是非活跃用户结束的生命周期
    @author: ldy
"""
import os
import gzip
from datetime import datetime, date, timedelta
import sqlite3 as sqlite
from inactive_conf import insert as db_insert, data_path

insert_str = "INSERT INTO period (target_date,max_day,min_day,average_day,Q1,Q2,Q3) VALUES (%s)" % ','.join(['%s']*7)
temp_new_user_db = '../tmp/new_user.db'
total_record = 0
unfind_record = 0


def _create_tmp_db():
    try:
        os.remove(temp_new_user_db)
    except:
        print "delete tmpdb fail, creating tmp db"
    creat_script = "CREATE TABLE newer(id INTEGER PRIMARY KEY, did char UNIQUE NOT NULL, log_date char NOT NULL);"
    con = sqlite.connect(temp_new_user_db)
    with con:
        cur = con.cursor()
        cur.execute(creat_script)


def _update_tmp_db(target_file):
    target_date = target_file.split('_', 3)[2]
    new_user_set = set()
    print target_file
    try:
        with gzip.open(data_path['newb'] + target_file, 'r') as f_in:
            for line in f_in:
                new_user_set.add(line.strip())
    except:
        print "not gzip format"
        with open(data_path['newb'] + target_file, 'r') as f_in:
            for line in f_in:
                new_user_set.add(line.strip())
    con = sqlite.connect(temp_new_user_db)
    with con:
        cur = con.cursor()
        for ele in new_user_set:
            cur.execute("INSERT INTO newer (did, log_date) VALUES (?,?)", (ele, target_date))


def _init_tmp_db():
    file_list = []
    for filename in os.listdir(data_path['newb']):
        if filename.endswith("ru-ru.txt.gz"):
            file_list.append(filename)
    for ele in file_list:
        _update_tmp_db(ele)


def _generate_date(inactive_user_dict, assign_date, max_diff):
    global total_record
    global unfind_record
    a = datetime.strptime(assign_date, '%Y%m%d').date()
    con = sqlite.connect(temp_new_user_db)
    find_script = "SELECT log_date FROM newer WHERE did=?"
    with con:
        cur = con.cursor()
        for ele in inactive_user_dict:
            total_record += 1
            cur.execute(find_script, (ele,))
            newer_date = cur.fetchone()
            # print newer_date
            if newer_date is None:
                print "unfind %s" % ele
                unfind_record += 1
                inactive_user_dict[ele] = -1
                continue
            b = datetime.strptime(newer_date[0], '%Y-%m-%d').date()
            diff = (a - b).days
            if diff < 0:
                print 'warn did: %s, new_date: %s, inactive_date: %s' % (ele, newer_date[0], assign_date)
                diff = max_diff
            inactive_user_dict[ele] = diff


def _count(inactive_user_dict):
    date_list = [inactive_user_dict[x] for x in inactive_user_dict if inactive_user_dict[x]!= -1]
    length = len(date_list)
    max_day = max(date_list)
    min_day = min(date_list)
    average_day = sum(date_list) / length
    date_list.sort()
    Q1 = date_list[length/4]
    Q2 = date_list[length/2]
    Q3 = date_list[3*length/4]
    return [max_day, min_day, average_day, Q1, Q2, Q3]


def one_life_time(inactive_user_path, assign_date):
    a = datetime.strptime(assign_date, '%Y%m%d').date()
    max_diff = (a - datetime.strptime('20150206', '%Y%m%d').date()).days
    inactive_user_dict = {}
    with open(inactive_user_path, 'r') as f_in:
        for line in f_in:
            [did, is_active] = line.strip().split('\t')[0:2]
            if is_active != '1':
                inactive_user_dict[did] = max_diff
    _generate_date(inactive_user_dict, assign_date, max_diff)
    data = _count(inactive_user_dict)
    data.insert(0, assign_date)
    # with open('./tmp/newer_life_time.out', 'a') as f_out:
    #     f_out.write(assign_date + '\tru-ru\t' + '\t'.join(str(x) for x in data) + '\n')
    return [data]


def daily(assign_date=date.today()):
    format_date = (date.today()).strftime('%Y%m%d')
    new_log_date = (assign_date - timedelta(days=1)).strftime('%Y-%m-%d')
    print "assign date: %s" % assign_date
    _update_tmp_db('new_user_%s_ru-ru.txt.gz' % new_log_date)
    inactive_user_path = data_path['group'] + 'user_group-%s.txt' % format_date
    res = one_life_time(inactive_user_path, format_date)
    db_insert(res, insert_str)


def init_db(num):
    _create_tmp_db()
    _init_tmp_db()
    for i in range(num):
        assign_date = (date.today() - timedelta(days=i)).strftime('%Y%m%d')
        print "assign date: %s" % assign_date
        inactive_user_path = data_path['group'] + 'user_group-%s.txt' % assign_date
        res = one_life_time(inactive_user_path, assign_date)
        db_insert(res, insert_str)
    print "total: %s, unfind: %s" % (total_record, unfind_record)


if __name__ == '__main__':
    # init_db(38)
    daily()




