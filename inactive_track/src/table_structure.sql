CREATE database inactive;
USE inactive
CREATE TABLE  period(
    id INT unsigned not null auto_increment primary key,
    target_date DATE NOT NULL,
    locale char(8) NOT NULL DEFAULT 'ru-ru',
    max_day SMALLINT NOT NULL DEFAULT 0,
    min_day SMALLINT NOT NULL DEFAULT 0,
    average_day SMALLINT NOT NULL DEFAULT 0,
    Q1 SMALLINT NOT NULL DEFAULT 0,
    Q2 SMALLINT NOT NULL DEFAULT 0,
    Q3 SMALLINT NOT NULL DEFAULT 0
);

CREATE TABLE conversion_rate(
    id INT unsigned NOT NULL auto_increment primary key,
    target_date DATE NOT NULL,
    locale char(8) NOT NULL DEFAULT 'ru-ru',
    inactive float NOT NULL DEFAULT 0,
    active float NOT NULL DEFAULT 0,
    new float NOT NULL DEFAULT 0
);
CREATE TABLE top_news_click(
    id INT unsigned NOT NULL auto_increment primary key,
    target_date DATE NOT NULL,
    locale char(8) NOT NULL DEFAULT 'ru-ru',
    pc10 float NOT NULL DEFAULT 0,
    pc20 float NOT NULL DEFAULT 0,
    pc30 float NOT NULL DEFAULT 0,
    pc40 float NOT NULL DEFAULT 0,
    pc50 float NOT NULL DEFAULT 0,
    pc60 float NOT NULL DEFAULT 0,
    pc70 float NOT NULL DEFAULT 0,
    pc80 float NOT NULL DEFAULT 0,
    pc90 float NOT NULL DEFAULT 0,
    pc100 float NOT NULL DEFAULT 0,
    other float NOT NULL DEFAULT 0
);
CREATE TABLE news_category(
    id INT unsigned NOT NULL auto_increment primary key,
    target_date DATE NOT NULL,
    locale char(8) NOT NULL DEFAULT 'ru-ru',
    category_id int NOT NULL DEFAULT 0,
    num int NOT NULL DEFAULT 0
);


CREATE TABLE ctr(
    id INT unsigned NOT NULL auto_increment primary key,
    target_date DATE NOT NULL,
    locale char(8) NOT NULL DEFAULT 'ru-ru',
    groups char(8) NOT NULL DEFAULT 'inactive',
    ctr_1 float NOT NULL DEFAULT 0,
    ctr_2 float NOT NULL DEFAULT 0,
    ctr_3 float NOT NULL DEFAULT 0,
    ctr_4 float NOT NULL DEFAULT 0,
    ctr_5 float NOT NULL DEFAULT 0,
    ctr_6 float NOT NULL DEFAULT 0,
    ctr_7 float NOT NULL DEFAULT 0,
    ctr_8 float NOT NULL DEFAULT 0,
    ctr_9 float NOT NULL DEFAULT 0,
    ctr_10 float NOT NULL DEFAULT 0
);
