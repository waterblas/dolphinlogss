# -*- coding: utf-8 -*-
"""
    统计非活跃用户的点击在包含活跃用户+新用户点击的新闻top500新闻排行榜的位置分布
    @author: ldy
"""
from datetime import date, timedelta
from inactive_conf import insert as db_insert, data_path

top_n = 500
insert_str = "INSERT INTO top_news_click (target_date,pc10,pc20,pc30,pc40,pc50," \
             "pc60,pc70,pc80,pc90,pc100,other) VALUES (%s)" % ','.join(['%s'] * 12)


def _get_news_weight(exclude_users, file_path):
    other_weight = {}
    inactive_weight = {}
    with open(file_path, 'r') as f_in:
        table_header = f_in.readline().split('\t')
        # print table_header
        for line in f_in:
            temp = line.split('\t')
            row = {table_header[i]: temp[i] for i in range(0, len(temp))}
            if row.get('Action', None) != 'news.click':
                continue
            news_item_id = row['Item']
            if row['Did'] not in exclude_users:
                if news_item_id in other_weight:
                    other_weight[news_item_id] += 1
                else:
                    other_weight[news_item_id] = 1
            else:
                if news_item_id in inactive_weight:
                    inactive_weight[news_item_id] += 1
                else:
                    inactive_weight[news_item_id] = 1
    print 'inactive news count: %s' % len(inactive_weight)
    return other_weight, inactive_weight


def _get_inactive_user(file_path):
    inactive_set = set()
    with open(file_path, 'r') as f_in:
        for line in f_in:
            [did, is_active] = line.strip().split('\t')[0:2]
            if is_active != '1':
                inactive_set.add(did)
    return inactive_set


def _get_top_n_news(item_weight, n):
    news_sorted = sorted(item_weight.iteritems(), key=lambda d: -d[1])[:n]
    return _order_top_news(news_sorted)


def _order_top_news(news_sorted):
    news_order = {}
    i = 0
    for ele in news_sorted:
        news_order[ele[0]] = i
        i += 1
    print 'top news count: %s' % i
    return news_order


def _get_inactive_click_percent(parts, top_news, inactive_weight):
    res = [0]*(parts + 1)
    times = len(top_news) / parts
    total = 0
    for ele in inactive_weight:
        total += inactive_weight[ele]
        if ele not in top_news:
            res[parts] += inactive_weight[ele]
        else:
            locale = top_news[ele] / times
            res[locale] += inactive_weight[ele]
    return [x * 1.0 / total for x in res]


def daily(target_day=date.today()):
    format_day = target_day.strftime('%Y%m%d')
    print 'extract day %s' % format_day
    inactive_set = _get_inactive_user(data_path['group'] + 'user_group-%s.txt' % format_day)
    print 'read inactive list over'
    other_weight, inactive_weight = _get_news_weight(inactive_set,  data_path['parsed'] + '%s.parsed' % format_day)
    print 'read news info over'
    top_news_n = _get_top_n_news(other_weight, top_n)
    res = _get_inactive_click_percent(10, top_news_n, inactive_weight)
    res.insert(0, format_day)
    print 'result: ', res
    db_insert([res], insert_str)


def init_db(num):
    for i in range(num):
        daily(date.today() - timedelta(days=i))


if __name__ == '__main__':
    daily()
    # init_db(39)














