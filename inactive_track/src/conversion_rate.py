# -*- coding: utf-8 -*-
"""
    统计前一天的用户(区分活跃用户/非活跃用户/新用户)到非活跃用户的转换来源
    @author: ldy
"""
import gzip
from datetime import date, timedelta
from inactive_conf import insert as db_insert, data_path

insert_str = "INSERT INTO conversion_rate (target_date,active, inactive, new) VALUES (%s,%s,%s,%s)"


def _convert_count(assign_date):
    assign_inactive = []
    previous_newers = set()
    previous_active = set()
    previous_inactive = set()

    previous_date = (assign_date - timedelta(days=1)).strftime('%Y%m%d')
    newer_log_date = (assign_date - timedelta(days=2)).strftime('%Y-%m-%d')
    with gzip.open(data_path['newb'] + 'new_user_%s_ru-ru.txt.gz' % newer_log_date, 'r') as f_in:
        for line in f_in:
            previous_newers.add(line.strip())
    with open(data_path['group'] + 'user_group-%s.txt' % previous_date, 'r') as f_in:
        for line in f_in:
            [did, is_active] = line.strip().split('\t')[0:2]
            if is_active == '1':
                previous_active.add(did)
            else:
                previous_inactive.add(did)
    with open(data_path['group'] + 'user_group-%s.txt' % assign_date.strftime('%Y%m%d'), 'r') as f_in:
        for line in f_in:
            [did, is_active] = line.strip().split('\t')[0:2]
            if is_active != '1':
                assign_inactive.append(did)

    print "read over"
    total = len(assign_inactive)
    print "count %s" % total
    active_num = 0
    inactive_num = 0
    new_num = 0
    for did in assign_inactive:
        if did in previous_active:
            active_num += 1
        if did in previous_inactive:
            inactive_num += 1
        if did in previous_newers:
            new_num += 1
    return assign_date, active_num * 1.0 / total, inactive_num * 1.0 / total, new_num * 1.0 / total


def daily(assign_date=date.today()):
    data = _convert_count(assign_date)
    db_insert([data], insert_str)


def init_db(num):
    for i in range(num):
        assign_date = date.today() - timedelta(days=i)
        daily(assign_date)


if __name__ == '__main__':
    daily()
    # init_db(20)
