# -*- coding: utf-8 -*-
"""
    Initialize history record, and save the results into db
    @author: ldy
"""
from top_news_click import init_db as top_news_init
from news_category import init_db as category_init
from inactive_period import init_db as period_init
from conversion_rate import init_db as conversion_init
from ctr import init_db as ctr_init

top_news_init(35)
category_init(35)
period_init(35)
conversion_init(35-1)
ctr_init(35)
