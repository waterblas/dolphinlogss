# -*- coding: utf-8 -*-
"""
    统计inactive user click新闻的分类分布
    @author: ldy
"""
from datetime import date, timedelta
from pymongo import MongoClient
from inactive_conf import insert as db_insert, data_path

con = MongoClient('52.28.22.114')
weibo_db = con['weibo_ru_ru']
category_dict = {}
insert_str = "INSERT INTO news_category (target_date,category_id, num) VALUES (%s)" % ','.join(['%s'] * 3)


def get_category(info_id):
    if info_id in category_dict:
        return category_dict[info_id]
    info = weibo_db.infos.find_one({'_id': info_id}, {'category': 1})
    category_dict[info_id] = info['category']
    return info['category'] if info is not None else None


def _get_inactive_user(file_path):
    inactive_set = set()
    with open(file_path, 'r') as f_in:
        for line in f_in:
            [did, is_active] = line.strip().split('\t')[0:2]
            if is_active != '1':
                inactive_set.add(did)
    return inactive_set


def _get_inactive_news(inactive_users, file_path):
    inactive_news = {}
    with open(file_path, 'r') as f_in:
        table_header = f_in.readline().split('\t')
        # print table_header
        for line in f_in:
            temp = line.split('\t')
            row = {table_header[i]: temp[i] for i in range(0, len(temp))}
            if row.get('Action', None) != 'news.click' or row.get('Did', None) not in inactive_users or row.get('Item', None) is None:
                continue
            if row['Item'] in inactive_news:
                inactive_news[row['Item']] += 1
            else:
                inactive_news[row['Item']] = 1
    return inactive_news


def classify_news(inactive_news):
    res = {}
    for info_id in inactive_news:
        category_id = get_category(int(info_id))
        if category_id is None:
            continue
        if category_id in res:
            res[category_id] += inactive_news[info_id]
        else:
            res[category_id] = inactive_news[info_id]
    return res


def _format_res(res, format_day):
    final_res = []
    for ele in res:
        final_res.append(tuple([format_day, ele, res[ele]]))
    return final_res


def daily(target_day=date.today()):
    format_day = target_day.strftime('%Y%m%d')
    inactive_user = _get_inactive_user(data_path['group'] + 'user_group-%s.txt' % format_day)
    inactive_news = _get_inactive_news(inactive_user, data_path['parsed'] + '%s.parsed.bak' % format_day)
    res = classify_news(inactive_news)
    db_insert(_format_res(res, format_day), insert_str)
    print res


def init_db(num):
    for i in range(num):
        daily(target_day=date.today() - timedelta(days=i))

if __name__ == '__main__':
    daily(date.today() - timedelta(days=24))


