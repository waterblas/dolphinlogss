__author__ = 'ldy'
import MySQLdb

data_path = {
#    'group': '/home/qwang/stat2/groups/',
#    'parsed': '/home/qwang/stat2/result/',
    'group': '/home/ldy/Documents/stat2/groups/',
    'parsed': '/home/ldy/Documents/stat2/result/',
    'newb': '/mnt/data_eg/new_user/'
}
db_host = 'localhost'
db_user = 'root'
db_pw = '12345'
db_name = 'inactive'


def insert(res, insert_str, db_name='inactive'):
    db = MySQLdb.connect(host=db_host, user=db_user, passwd=db_pw, db=db_name)
    with db:
        cur = db.cursor()
        cur.executemany(insert_str, res)
    print 'save over'
